/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * httxt2dbm.c: simple program for converting RewriteMap text files to DBM
 * Rewrite databases for the Apache HTTP server
 *
 */

#include "apr.h"
#include "apr_version.h"
#include "apr_lib.h"
#include "apr_strings.h"
#include "apr_file_io.h"
#include "apr_file_info.h"
#include "apr_pools.h"
#include "apr_getopt.h"
#include "apu.h"
#include "apr_dbm.h"

#if APR_HAVE_STDLIB_H
#include <stdlib.h> /* for atexit() */
#endif

#if APR_VERSION_AT_LEAST(1,5,0)
# include "apr_escape.h"
# define HAVE_APR_ESCAPE
#else

int
urldecode(
    char        *url,
    apr_size_t  *url_len
)
{
    apr_size_t  l_in = *url_len, l_out = 0;
    char        *dst = url;
  
    while ( l_in && *url ) {
        unsigned char   c = *url;
      
        switch ( c ) {
        
            case '%': {
              
                if ( l_in < 3 ) return -1;
                url++; l_in--;
                if ( ! (*url >= '0' && *url <= '9') && ! ((*url >= 'A' && *url <= 'F') || (*url >= 'a' && *url <= 'f')) ) return -1;
                c = (*url - '0') * 16;
                url++; l_in--;
                if ( ! (*url >= '0' && *url <= '9') && ! ((*url >= 'A' && *url <= 'F') || (*url >= 'a' && *url <= 'f')) ) return -1;
                c += *url - '0';
                if ( c == 0 || c == 0x2f ) return -1;
                break;
            }
        
            default: {
                break;
            }
        }
        *dst = c;
        dst++;
        url++;
        l_in--;
        l_out++;
      
    }
    *dst = '\0';
    *url_len = l_out;
    return 0;
}

#endif

static const char *input;
static const char *output;
static const char *format;
static const char *shortname;
static apr_file_t *errfile;
static char errbuf[120];
static int verbose;
static int unescape;

/* From mod_rewrite.c */
#ifndef REWRITE_MAX_TXT_MAP_LINE
#define REWRITE_MAX_TXT_MAP_LINE 1024
#endif

#define NL APR_EOL_STR

#define AVAIL "available"
#define UNAVAIL "unavailable"

static void usage(void)
{
    const char *have_sdbm;
    const char *have_gdbm;
    const char *have_ndbm;
    const char *have_db;

#if APU_HAVE_SDBM
    have_sdbm = AVAIL;
#else
    have_sdbm = UNAVAIL;
#endif
#if APU_HAVE_GDBM
    have_gdbm = AVAIL;
#else
    have_gdbm = UNAVAIL;
#endif
#if APU_HAVE_NDBM
    have_ndbm = AVAIL;
#else
    have_ndbm = UNAVAIL;
#endif
#if APU_HAVE_DB
    have_db = AVAIL;
#else
    have_db = UNAVAIL;
#endif

    apr_file_printf(errfile,
    "%s -- Program to Create DBM Files for use by RewriteMap" NL
    "Usage: %s [-vu] [-f format] -i SOURCE_TXT -o OUTPUT_DBM" NL
    NL
    "Options: " NL
    " -v    More verbose output"NL
    " -u    unescape the values read from the source text file"NL
#ifdef HAVE_APR_ESCAPE
    "       using the apr_unescape_url() function"NL
#else
    "       using a simple percent-decode function"NL
#endif
    NL
    " -i    Source Text File. If '-', use stdin."NL
    NL
    " -o    Output DBM."NL
    NL
    " -f    DBM Format.  If not specified, will use the APR Default." NL
    "           GDBM for GDBM files (%s)" NL
    "           SDBM for SDBM files (%s)" NL
    "           DB   for berkeley DB files (%s)" NL
    "           NDBM for NDBM files (%s)" NL
    "           default for the default DBM type" NL
    NL,
    shortname,
    shortname,
    have_gdbm,
    have_sdbm,
    have_db,
    have_ndbm);
}


static apr_status_t to_dbm(apr_dbm_t *dbm, apr_file_t *fp, apr_pool_t *pool)
{
    apr_status_t rv = APR_SUCCESS;
    char line[REWRITE_MAX_TXT_MAP_LINE + 1]; /* +1 for \0 */
    apr_datum_t dbmkey;
    apr_datum_t dbmval;
    apr_pool_t* p;

    apr_pool_create(&p, pool);

    while (apr_file_gets(line, sizeof(line), fp) == APR_SUCCESS) {
        char *c, *value, *lptr = (char*)line, *dptr;
        char delim, prevc;
        apr_size_t len;
        apr_status_t rc;

        if (*lptr == '#' || apr_isspace(*lptr)) {
            continue;
        }
        switch (*lptr) {
            case '"':
            case '\'':
                delim=*lptr;
                lptr++;
                break;
            default:
                delim=0;
                break;
        }
        c = lptr;

        prevc = 0;
        while (*c && ((!delim && !apr_isspace(*c)) || (delim && (*c != delim || prevc == '\\')))) {
            prevc = *c;
            ++c;
        }

        if (!*c || (c - lptr == 0)) {
            /* no value. solid line of data. */
            continue;
        }

        dbmkey.dptr = apr_pstrmemdup(p, lptr,  c - lptr);
        dbmkey.dsize = (c - lptr);
        if ( delim ) {
            /* Drop any escaped quotes from the key: */
            len = dbmkey.dsize;
            dptr = lptr = dbmkey.dptr;
            prevc = 0;
            while ( len-- ) {
                if ( (*lptr == delim) && (prevc == '\\') ) {
                    dptr--;
                    dbmkey.dsize--;
                }
                *dptr = prevc = *lptr;
                dptr++;
                lptr++;
            }
            while ( dptr < lptr ) {
                *dptr = '\0';
                dptr++;
            }
        }
        if ( unescape ) {
#ifdef HAVE_APR_ESCAPE
            rc = apr_unescape_url(NULL, dbmkey.dptr, dbmkey.dsize, NULL, NULL, 0, &len);
            switch ( rc ) {
            
                case APR_SUCCESS:
                    if ( (lptr = apr_palloc(p, len + 1)) ) {
                        apr_unescape_url(lptr, dbmkey.dptr, dbmkey.dsize, NULL, NULL, 0, &len);
                        dbmkey.dptr = lptr;
                        dbmkey.dsize = len;
                    } else {
                      fprintf(stderr, "WARNING:  failed to allocate decoded URL (left-hand-side):  %s"NL, dbmkey.dptr);
                      apr_pool_clear(p);
                      continue;
                    }
                case APR_NOTFOUND:
                    /* No decoding necessary: */
                    break;
                
                default:
                    fprintf(stderr, "WARNING:  failed to decode URL (left-hand-side):  %s"NL, dbmkey.dptr);
                    apr_pool_clear(p);
                    continue;
            }
#else
            rc = urldecode(dbmkey.dptr, &dbmkey.dsize);
            if ( rc != 0 ) {
                fprintf(stderr, "WARNING:  failed to decode URL (left-hand-side):  %s"NL, dbmkey.dptr);
                apr_pool_clear(p);
                continue;
            }
#endif
        }

        if ( *c == delim ) {
            c++;
        }

        while (*c && apr_isspace(*c)) {
            ++c;
        }

        if (!*c) {
            apr_pool_clear(p);
            continue;
        }

        switch (*c) {
            case '"':
            case '\'':
                delim=*c;
                c++;
                break;
            default:
                delim=0;
                break;
        }
        value = c;

        prevc = 0;
        while (*c && ((!delim && !apr_isspace(*c)) || (delim && (*c != delim || prevc == '\\')))) {
            prevc = *c;
            ++c;
        }

        if ( c - value == 0 ) {
            apr_pool_clear(p);
            continue;
        }

        dbmval.dptr = apr_pstrmemdup(p, value,  c - value);
        dbmval.dsize = (c - value);
        if ( delim ) {
            /* Drop any escaped quotes from the value: */
            len = dbmval.dsize;
            dptr = lptr = dbmval.dptr;
            prevc = 0;
            while ( len-- ) {
                if ( (*lptr == delim) && (prevc == '\\') ) {
                    dptr--;
                    dbmval.dsize--;
                }
                *dptr = prevc = *lptr;
                dptr++;
                lptr++;
            }
            while ( dptr < lptr ) {
                *dptr = '\0';
                dptr++;
            }
        }
        if ( unescape ) {
#ifdef HAVE_APR_ESCAPE
            rc = apr_unescape_url(NULL, dbmval.dptr, dbmval.dsize, NULL, NULL, 0, &len);
            switch ( rc ) {
            
                case APR_SUCCESS:
                    if ( (lptr = apr_palloc(p, len + 1)) ) {
                        apr_unescape_url(lptr, dbmval.dptr, dbmval.dsize, NULL, NULL, 0, &len);
                        dbmval.dptr = lptr;
                        dbmval.dsize = len;
                    } else {
                      fprintf(stderr, "WARNING:  failed to allocate decoded URL (right-hand-side):  %s"NL, dbmkey.dptr);
                      apr_pool_clear(p);
                      continue;
                    }
                case APR_NOTFOUND:
                    /* No decoding necessary: */
                    break;
                
                default:
                    fprintf(stderr, "WARNING:  failed to decode URL (right-hand-side):  %s"NL, dbmkey.dptr);
                    apr_pool_clear(p);
                    continue;
            }
#else
            rc = urldecode(dbmval.dptr, &dbmval.dsize);
            if ( rc != 0 ) {
                fprintf(stderr, "WARNING:  failed to decode URL (right-hand-side):  %s"NL, dbmval.dptr);
                apr_pool_clear(p);
                continue;
            }
#endif
        }

        if (verbose) {
            apr_file_printf(errfile, "    '%s' -> '%s'"NL,
                            dbmkey.dptr, dbmval.dptr);
        }

        rv = apr_dbm_store(dbm, dbmkey, dbmval);

        apr_pool_clear(p);

        if (rv != APR_SUCCESS) {
            break;
        }
    }

    return rv;
}

int main(int argc, const char *const argv[])
{
    apr_pool_t *pool;
    apr_status_t rv = APR_SUCCESS;
    apr_getopt_t *opt;
    const char *optarg;
    char ch;
    apr_file_t *infile;
    apr_dbm_t *outdbm;

    apr_app_initialize(&argc, &argv, NULL);
    atexit(apr_terminate);

    verbose = 0;
    unescape = 0;
    format = NULL;
    input = NULL;
    output = NULL;

    apr_pool_create(&pool, NULL);

    if (argc) {
        shortname = apr_filepath_name_get(argv[0]);
    }
    else {
        shortname = "httxt2dbm";
    }

    apr_file_open_stderr(&errfile, pool);
    rv = apr_getopt_init(&opt, pool, argc, argv);

    if (rv != APR_SUCCESS) {
        apr_file_printf(errfile, "Error: apr_getopt_init failed."NL NL);
        return 1;
    }

    if (argc <= 1) {
        usage();
        return 1;
    }

    while ((rv = apr_getopt(opt, "uvf::i::o::", &ch, &optarg)) == APR_SUCCESS) {
        switch (ch) {
        case 'u':
            if (unescape) {
                apr_file_printf(errfile, "Error: -u can only be passed once" NL NL);
                usage();
                return 1;
            }
            unescape = 1;
            break;
        case 'v':
            if (verbose) {
                apr_file_printf(errfile, "Error: -v can only be passed once" NL NL);
                usage();
                return 1;
            }
            verbose = 1;
            break;
        case 'f':
            if (format) {
                apr_file_printf(errfile, "Error: -f can only be passed once" NL NL);
                usage();
                return 1;
            }
            format = apr_pstrdup(pool, optarg);
            break;
        case 'i':
            if (input) {
                apr_file_printf(errfile, "Error: -i can only be passed once" NL NL);
                usage();
                return 1;
            }
            input = apr_pstrdup(pool, optarg);
            break;
        case 'o':
            if (output) {
                apr_file_printf(errfile, "Error: -o can only be passed once" NL NL);
                usage();
                return 1;
            }
            output = apr_pstrdup(pool, optarg);
            break;
        }
    }

    if (rv != APR_EOF) {
        apr_file_printf(errfile, "Error: Parsing Arguments Failed" NL NL);
        usage();
        return 1;
    }

    if (!input) {
        apr_file_printf(errfile, "Error: No input file specified." NL NL);
        usage();
        return 1;
    }

    if (!output) {
        apr_file_printf(errfile, "Error: No output DBM specified." NL NL);
        usage();
        return 1;
    }

    if (!format) {
        format = "default";
    }

    if (verbose) {
        apr_file_printf(errfile, "DBM Format: %s"NL, format);
    }

    if (!strcmp(input, "-")) {
        rv = apr_file_open_stdin(&infile, pool);
    }
    else {
        rv = apr_file_open(&infile, input, APR_READ|APR_BUFFERED,
                           APR_OS_DEFAULT, pool);
    }

    if (rv != APR_SUCCESS) {
        apr_file_printf(errfile,
                        "Error: Cannot open input file '%s': (%d) %s" NL NL,
                         input, rv, apr_strerror(rv, errbuf, sizeof(errbuf)));
        return 1;
    }

    if (verbose) {
        apr_file_printf(errfile, "Input File: %s"NL, input);
    }

    rv = apr_dbm_open_ex(&outdbm, format, output, APR_DBM_RWCREATE,
                    APR_OS_DEFAULT, pool);

    if (APR_STATUS_IS_ENOTIMPL(rv)) {
        apr_file_printf(errfile,
                        "Error: The requested DBM Format '%s' is not available." NL NL,
                         format);
        return 1;
    }

    if (rv != APR_SUCCESS) {
        apr_file_printf(errfile,
                        "Error: Cannot open output DBM '%s': (%d) %s" NL NL,
                         output, rv, apr_strerror(rv, errbuf, sizeof(errbuf)));
        return 1;
    }

    if (verbose) {
        apr_file_printf(errfile, "DBM File: %s"NL, output);
    }

    rv = to_dbm(outdbm, infile, pool);

    if (rv != APR_SUCCESS) {
        apr_file_printf(errfile,
                        "Error: Converting to DBM: (%d) %s" NL NL,
                         rv, apr_strerror(rv, errbuf, sizeof(errbuf)));
        return 1;
    }

    apr_dbm_close(outdbm);

    if (verbose) {
        apr_file_printf(errfile, "Conversion Complete." NL);
    }

    return 0;
}

