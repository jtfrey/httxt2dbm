#
# This Makefile assumes you have apr-1-config and apu-1-config
# on your PATH.
#

TARGET		= httxt2dbm

OBJECTS		= httxt2dbm.o

PREFIX		= /usr/local

#

CC		= $(shell apr-1-config --cc)

CFLAGS		+= $(shell apr-1-config --cflags)
CPPFLAGS	+= $(shell apr-1-config --cppflags --includes) $(shell apu-1-config --includes)

LDFLAGS		+= $(shell apr-1-config --ldflags) $(shell apu-1-config --ldflags)
LIBS		+= $(shell apr-1-config --link-ld --libs) $(shell apu-1-config --link-ld --dbm-libs)

#

all: $(TARGET)

clean:
	$(RM) $(TARGET) $(OBJECTS) testing-map.dbm*

test: $(TARGET) testing-map.dbm

install: $(TARGET)
	install --owner=root --group=root --mode=0755 $(TARGET) $(PREFIX)/bin/$(TARGET)

#

$(TARGET): $(OBJECTS)
	$(CC) -o $(TARGET) $(OBJECTS) $(LDFLAGS) $(LIBS)

%.o: %.c
	$(CC) -c $< $(CPPFLAGS) $(CFLAGS)

#

testing-map.dbm: testing-map.txt
	./$(TARGET) -vu -i testing-map.txt -o testing-map.dbm
	touch testing-map.dbm

