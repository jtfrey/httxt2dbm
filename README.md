httxt2dbm
=========

The httxt2dbm utility bundled with Apache takes a textual URL map (like mod_rewrite itself would use for a 'txt' RewriteMap) and exports it as a DBM file.  Like RewriteMap, the utility parses a single URL pair per line, separated by whitespace.  So a line like

```
/this/url/has spaces                http://www.othersite.com/has spaces
```

gets parsed as

```
/this/url/has      =>      spaces
```

One might be tempted to use RFC3986 encoding:

```
/this/url/has%20spaces              http://www.othersite.com/has%20spaces
```

but the rewrite engine receives RFC3986-decoded URLs which won't match that left-hand-side.  One solution would be to apply a built-in RFC3986-encoding rewrite map to the value before passing it to the rewrite map:

```
RewriteMap RFC3986_Encode int:escape
RewriteMap legacy-internal dbm:uri-maps/live/legacy-internal.dbm

RewriteCond %{REQUEST_URI}                                     ^/testing(/.*)/?$
RewriteCond ${legacy-internal:${RFC3986_Encode:%1}|NOT_FOUND}  ^((?!NOT_FOUND).*)$
RewriteRule ^.*$                                               %0 [R=301,L]
```

There's no reason the DBM keys and values cannot have whitespace in them, though, and all that re-encoding of the URL will eat CPU cycles.

This version of httxt2dbm allows for quoting in the textual file:

```
"/this/url/has spaces"             "http://www.othersite.com/has spaces"
"/this/url/has\"quotes\""          'http://www.othersite.com/has\'quotes\''
```

If the character leading the left- or right-hand column is a single- or double-quote the parser looks for a matching quote character to terminate the string.  Inclusion of the delimiting quote in the string should be escaped with a preceding backslash.  Lacking a leading quote, parsing is handled as before (whitespace delimited).

Building the utility
====================

A Makefile is included that assumes `apr-1-config` and `apu-1-config` are on the PATH.  Just type `make` to build the utility.  A simple URL map is included to test the utility:

```
$ make test
./httxt2dbm -v -i testing-map.txt -o testing-map.dbm
DBM Format: default
Input File: testing-map.txt
DBM File: testing-map.dbm
    '/let%20me%20in' -> 'http://www.udel.edu/xyz/'barf.txt'?g=h'
    '/let_me_in' -> 'http://www.udel.edu/'
    '/let me "in"' -> 'http://www.udel.edu/abc/'
Conversion Complete.
touch testing-map.dbm
```

The Makefile also assumes an install prefix of `/usr/local` for its `install` target.

Additional CLI options
======================

One additional CLI option has been added:

|option|description|
|------|-----------|
| `-u` | Apply URL unescaping to the left- and right-hand-sides of each line read from the input file |

This option complements the quoting feature introduced in this version; percent-encoded characters are replaced with the actual character, allowing the following form for whitespace:

```
/this/url/has%20spaces              http://www.othersite.com/has%20spaces
```
